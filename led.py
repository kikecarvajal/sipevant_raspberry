from led import classLed
import time

m = classLed.raspberry4('board', 7)
m.defineTypeMode()
if m.valueInit() == 1:
    try:
       for i in range(10):
           m.on()
           time.sleep(2)
           m.off()
           time.sleep(1)
    except KeyboardInterrupt:
            print("Salida")
m.clearBuffer()
