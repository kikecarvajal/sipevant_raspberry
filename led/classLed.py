import RPi.GPIO as GPIO
class raspberry4():
    def __init__(self, mode_motherboard, pin):
        '''
        mode_motherboard = Como se va numerar los pines de las raspberry <<BOARD>> ó <<BCM>>\n
        pin = Numero de pin que va tener la salida.\n
        '''
        self.__pin = pin
        self.__mode_motherboard = mode_motherboard

    def defineTypeMode(self):
        '''
        Agrega GPIO al tipo de como se va usar las raspberry
        '''
        try:
            if self.__mode_motherboard.upper() == 'BOARD':
                self.__mode_motherboard = "GPIO.BOARD"
                # return self.__mode_motherboard
            elif self.__mode_motherboard.upper() == 'BCM':
                self.__mode_motherboard = "GPIO.BCM"
                # return self.__mode_motherboard
            else:
                return "Los valores permitidos BOARD y BCM"
        except AttributeError:
            print("mode_motherboard debe ser texto")

    def valueInit(self):
        if self.__mode_motherboard == 'GPIO.BOARD':
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(self.__pin, GPIO.OUT)
            return 1
        else:
            return 0

    def on(self):
        GPIO.output(self.__pin, GPIO.HIGH)

    def off(self):
        GPIO.output(self.__pin, GPIO.LOW)

    
    def clearBuffer(self):
        GPIO.cleanup()